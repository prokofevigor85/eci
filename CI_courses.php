<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Courses extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    /*
	List
	*/
    public function index() {
        $out = array();
        $data = array();

        $this->load->model("courses_model");
        $this->load->model("pages_model");
        $text = $this->pages_model->get_item("where", "courses");
        $list = $this->courses_model->get_list();
        $data["text"] = $text;
        $data["list"] = $list;
        $data["right_col"] = $this->courses_model->get_news_other_pages();
        $out["data"] = $this->load->view("courses/list", $data, true);
        $this->load->view("layout", $out);
    }

    /*
	View
	*/
    public function show_item($i_id) {
        $out = array();
        $data = array();

        $this->load->model("courses_model");
        //data about course
        $item = $this->courses_model->get_item($i_id);
        
        //Enroll user is it right?
        $list = $this->courses_model->get_course_list($i_id);
        if (isset($_SESSION["user_id"]) && in_array($_SESSION["user_id"], $list)) {
            $data["sign_in"] = true;
        } else {
            $data["sign_in"] = false;
        }
        
        if ($item) {
            $data["item"] = $item;
            $data["right_col"] = $this->courses_model->get_news_other_pages();
            $out["data"] = $this->load->view("courses/show_item", $data, true);
        } else {
                $data["not_found"] = true;
                $out["data"] = $this->load->view("error", $data, true);
        }
        $this->load->view("layout", $out);
    }

    /*
	Editing
	*/
    public function edit_item($i_id) {
        if (!isset($_SESSION["state"]) || $_SESSION["state"] != "admin") {
            $data["no_access"] = true;
            $out["data"] = $this->load->view("error", $data, true);
            $this->load->view("layout", $out);
            return;
        }
        
        $out = array();
        $data = array();

        $this->load->model("courses_model");
        $item = $this->courses_model->get_item($i_id);

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Название',
                'rules' => 'trim|required|max_length[225]'
            ),
            array(
                'field' => 'content',
                'label' => 'Содержимое',
                'rules' => 'trim'
            ),
            array(
                'field' => 'anons',
                'label' => 'Анонс',
                'rules' => 'trim'
            ),
        );
        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<p class="validation_error">', '</p>');

        if ($this->form_validation->run() != FALSE) {
            $res = $this->courses_model->save_item();
            if (isset($res["error"])) {
                $data["error"] = $res["error"];
            } else {
                header("Location: " . site_url("/courses/show_item/$i_id"));
            }
        }

        if ($item) {
            $data["item"] = $item;
            $out["data"] = $this->load->view("courses/edit_item", $data, true);
        } else {
            $data["not_found"] = true;
            $out["data"] = $this->load->view("error", $data, true);
        }
        $this->load->view("layout", $out);
    }
    
    /*
	Creating
	*/
    function create_item() {
        if (!isset($_SESSION["state"]) || $_SESSION["state"] != "admin") {
            $data["no_access"] = true;
            $out["data"] = $this->load->view("error", $data, true);
            $this->load->view("layout", $out);
            return;
        }
        
        $out = array();
        $data = array();

        $this->load->model("courses_model");
        $item = array(
            "id" => 0,
            "name" => "",
            "content" => "",
            "anons" => "",
            "date" => array(date("Y"), date("m"), date("d")),
            "time" => date("H:i:s"),
            "programm" => "",
        );

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Название',
                'rules' => 'trim|required|max_length[225]'
            ),
            array(
                'field' => 'content',
                'label' => 'Содержимое',
                'rules' => 'trim'
            ),
            array(
                'field' => 'anons',
                'label' => 'Анонс',
                'rules' => 'trim'
            ),
        );
        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<p class="validation_error">', '</p>');

        if ($this->form_validation->run() != FALSE) {
            $res = $this->courses_model->save_item();
            if (isset($res["error"])) {
                $data["error"] = $res["error"];
            } else {
                header("Location: " . site_url("/courses/"));
            }
        }

        if ($item) {
            $data["item"] = $item;
            $out["data"] = $this->load->view("courses/edit_item", $data, true);
        } else {
            $data["not_found"] = true;
            $out["data"] = $this->load->view("error", $data, true);
        }
        $this->load->view("layout", $out);
    }
    
    /*
	Deleting
	*/
    function del_item($i_id) {
        if (!isset($_SESSION["state"]) || $_SESSION["state"] != "admin") {
            $data["no_access"] = true;
            $out["data"] = $this->load->view("error", $data, true);
            $this->load->view("layout", $out);
            return;
        }
        
        $out = array();
        $data = array();

        $this->load->model("courses_model");
        $item = $this->courses_model->get_item($i_id);
        if (isset($_POST["del_item"])) {
            $this->courses_model->del_item($i_id);
            header("Location: ".site_url("/courses"));
        }
        
        if ($item) {
            $data["item"] = $item;
            $out["data"] = $this->load->view("del_item", $data, true);
        } else {
            $data["not_found"] = true;
            $out["data"] = $this->load->view("error", $data, true);
        }
        $this->load->view("layout", $out);
    }
    
    /*
	Enroll to a course
	*/
    function sign_up($i_id) {
        if (!isset($_SESSION["state"]) || $_SESSION["state"] != "reserve") {
            $data["no_access"] = true;
            $out["data"] = $this->load->view("error", $data, true);
            $this->load->view("layout", $out);
            return;
        }
        
        $this->load->model("courses_model");
        $this->courses_model->sign_up($i_id);
        header("Location: ".  site_url("/courses/show_item/$i_id"));
    }
    
    /*
	List enroll to courses
	*/
    function users_list($course_id) {
        if (!isset($_SESSION["state"]) || $_SESSION["state"] != "admin") {
            $data["no_access"] = true;
            $out["data"] = $this->load->view("error", $data, true);
            $this->load->view("layout", $out);
            return;
        }
        
        $data = array();
        $out = array();

        $this->load->model("courses_model");
        $item = $this->courses_model->get_item($course_id);
        $users = $this->courses_model->get_users_list($course_id);
        
        if ($item) {
            $data["item"] = $item;
            $data["users"] = $users;
            $data["right_col"] = $this->courses_model->get_news_other_pages();
            $out["data"] = $this->load->view("courses/users_list", $data, true);
        } else {
            $data["not_found"] = true;
            $out["data"] = $this->load->view("error", $data, true);
        }
        $this->load->view("layout", $out);
    }
    
    function result_export($format, $course_id) {
        if (!isset($_SESSION["state"]) || $_SESSION["state"] != "admin") {
            $data["no_access"] = true;
            $out["data"] = $this->load->view("error", $data, true);
            $this->load->view("layout", $out);
            return;
        }
        
        $data = array();
        $out = array();

        $this->load->model("courses_model");
        $item = $this->courses_model->get_item($course_id);
        $data["res"] = $this->courses_model->get_users_list($course_id);
        
        if ($item) {
            $filename = 'file.'.$format;
            header('Content-Type: application/vnd.'.($format == "xls" ? "ms-excel" : "msword"));
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');

            print $this->load->view("courses/result_export", $data, true);
            
            return;
        } else {
            $data["not_found"] = true;
            $out["data"] = $this->load->view("error", $data, true);
        }
        $this->load->view("layout", $out);
    }

}
